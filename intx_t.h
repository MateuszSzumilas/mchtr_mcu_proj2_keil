#ifndef		INTX_T_H
#define		INTX_T_H

/* --------------------------- */

/* These types must be 8-bit integers */
typedef char					int8_t;
typedef unsigned char	uint8_t;
typedef unsigned char	BYTE;

/* These types must be 16-bit integers */
typedef short						int16_t;
typedef unsigned short	uint16_t;
typedef unsigned short	WORD;

/* These types must be 32-bit integers */
typedef long						int32_t;
typedef unsigned long		uint32_t;

/* ----------------------------*/

#endif		//INTX_T_H